<!DOCTYPE html>
<html>
<head>
	<title>Nueva cancion</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<br>
		<h1 align="center">.: Agregar nueva cancion :.</h1>
		<br>
		<hr>
		<br>
		<div id="alerta" >
		
		</div>
		<form id="formulario">
		  <div class="form-group" >
		    <label for="exampleInputEmail1">Genero</label>
		    <input type="text" class="form-control" name="genero" placeholder="Ingregar el genero">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Artista</label>
		    <input type="text" class="form-control" name="artista" placeholder="Ingrese el nombre del artista">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Canciones</label>
		    <input type="text" class="form-control" name="cancion" placeholder="Ingrese el nombre de la cancion">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Url</label>
		    <input type="text" class="form-control" name="urll" placeholder="Ingrese el Url de la cancion">
		  </div>
		  <button class="btn btn-info" type="submit">Agregar</button>
		  <a type="button" class="btn btn-info" href="catalogo.php">Volver </a>
		  <div id="respuesta">
		  </div>
		</form>
	</div>
	<script src="jquery-3.4.1.js" type="text/javascript"></script>   
	<script>
		var formulario= document.getElementById('formulario');
		var respuesta=document.getElementById('respuesta');
		var alerta=document.getElementById('alerta');
		formulario.addEventListener('submit', function(e){
          e.preventDefault();
          console.log('Me diste un click');

          var datos = new FormData(formulario)
         
          fetch('guardarcancion.php', {
              method: "POST",
              body: datos
          })


          .then(res=>res.json())
          .then(data=>{
              console.log(data)
			  alerta.innerHTML+=`
			  	<div class="alert alert-success" role="alert">
				  ${data}
				</div>
                  `

          })
      })
	</script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>